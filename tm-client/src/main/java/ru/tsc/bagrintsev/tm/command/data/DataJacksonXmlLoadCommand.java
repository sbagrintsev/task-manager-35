package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonXmlLoadRequest;

public final class DataJacksonXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().loadJacksonXml(new DataJacksonXmlLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from xml file";
    }

}
