package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;

public class VersionCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.printf("task-manager-client version: %s%n", getPropertyService().getApplicationVersion());
        System.out.printf("task-manager-server version: %s%n", getSystemEndpoint().getVersion(new VersionRequest()).getVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print version.";
    }

}
