package ru.tsc.bagrintsev.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.PROJECT_ID);
        @Nullable final String projectId = TerminalUtil.nextLine();
        showParameterInfo(EntityField.TASK_ID);
        @Nullable final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().unbindTaskFromProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }

}
