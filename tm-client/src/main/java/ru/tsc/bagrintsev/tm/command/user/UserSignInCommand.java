package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignInRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public final class UserSignInCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final String token = getAuthEndpoint().signIn(new UserSignInRequest(login, password)).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-sign-in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign user in.";
    }

}
