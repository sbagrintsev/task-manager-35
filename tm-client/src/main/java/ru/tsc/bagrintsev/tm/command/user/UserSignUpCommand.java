package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignUpRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class UserSignUpCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        showParameterInfo(EntityField.ROLE);
        System.out.println(Arrays.toString(Role.values()));
        @NotNull final Role role = Role.toRole(TerminalUtil.nextLine());
        showParameterInfo(EntityField.EMAIL);
        @NotNull final String email = TerminalUtil.nextLine();
        getUserEndpoint().signUp(new UserSignUpRequest(login, password, email));
    }

    @NotNull
    @Override
    public String getName() {
        return "user-sign-up";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign-up new user in system.";
    }

}
