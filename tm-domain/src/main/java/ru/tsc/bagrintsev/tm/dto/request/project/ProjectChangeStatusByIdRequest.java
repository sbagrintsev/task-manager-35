package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String statusValue;

    public ProjectChangeStatusByIdRequest(@Nullable String token) {
        super(token);
    }

}
