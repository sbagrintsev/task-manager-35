package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectShowByIdRequest(@Nullable String token) {
        super(token);
    }

}
