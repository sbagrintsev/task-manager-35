package ru.tsc.bagrintsev.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectShowByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectShowByIndexRequest(@Nullable String token) {
        super(token);
    }

}
