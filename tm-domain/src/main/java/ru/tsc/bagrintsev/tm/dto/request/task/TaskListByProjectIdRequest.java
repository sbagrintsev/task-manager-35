package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskListByProjectIdRequest(@Nullable String token) {
        super(token);
    }
}
