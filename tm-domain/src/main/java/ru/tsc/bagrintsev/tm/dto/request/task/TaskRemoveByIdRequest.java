package ru.tsc.bagrintsev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    public TaskRemoveByIdRequest(@Nullable String token) {
        super(token);
    }

}
