package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserLockResponse extends AbstractUserResponse {
}
