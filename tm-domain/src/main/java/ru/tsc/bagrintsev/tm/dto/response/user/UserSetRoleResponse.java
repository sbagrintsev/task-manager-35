package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.User;

@NoArgsConstructor
public final class UserSetRoleResponse extends AbstractUserResponse {

    public UserSetRoleResponse(@Nullable final User user) {
        super(user);
    }

}
