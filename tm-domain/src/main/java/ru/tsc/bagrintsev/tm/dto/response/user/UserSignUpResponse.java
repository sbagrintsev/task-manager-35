package ru.tsc.bagrintsev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.User;

@NoArgsConstructor
public final class UserSignUpResponse extends AbstractUserResponse {

    public UserSignUpResponse(@Nullable final User user) {
        super(user);
    }

}
