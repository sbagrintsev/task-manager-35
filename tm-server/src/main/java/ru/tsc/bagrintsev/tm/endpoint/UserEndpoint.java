package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.model.User;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String newPassword = request.getNewPassword();
        @Nullable final String oldPassword = request.getOldPassword();
        @NotNull final User user;
        try {
            user = getUserService().setPassword(userId, newPassword, oldPassword);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLockResponse lock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        @Nullable final String userId = check(request, Role.ADMIN).getUserId();
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserRemoveResponse remove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        @Nullable final String userId = check(request, Role.ADMIN).getUserId();
        @Nullable final String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserSetRoleResponse setRole(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSetRoleRequest request
    ) {
        @Nullable final String userId = check(request, Role.ADMIN).getUserId();
        @Nullable final String login = request.getLogin();
        @Nullable final Role role = request.getRole();
        @NotNull final User user = getUserService().setRole(login, role);
        return new UserSetRoleResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserSignUpResponse signUp(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserSignUpRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final User user = getUserService().create(login, password);
        getUserService().setParameter(user, EntityField.EMAIL, email);
        return new UserSignUpResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUnlockResponse unlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        @Nullable final String userId = check(request, Role.ADMIN).getUserId();
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final String lastName = request.getLastName();
        @NotNull final User user;
        try {
            user = getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
        return new UserUpdateProfileResponse(user);
    }

}
