package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectRepositoryTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private ProjectRepository projectRepository;

    @Before
    public void setUp() {
        projectRepository = new ProjectRepository();
    }

    @Test
    public void testAdd() {
        @NotNull final Project project = new Project();
        projectRepository.add(userId, project);
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(project, projectRepository.findAll().get(0));
        Assert.assertEquals(userId, projectRepository.findAll().get(0).getUserId());
    }

    @Test
    public void testAddCollection() {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.add(projectList);
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals(2, projectRepository.findAll().size());
        Assert.assertEquals(project2, projectRepository.findOneByIndex(1));
    }

    @Test
    public void testCreate() {
        projectRepository.create(userId, "name1");
        projectRepository.create(userId, "name2", "description2");
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertEquals("name1", projectRepository.findAll().get(0).getName());
        Assert.assertEquals("description2", projectRepository.findAll().get(1).getDescription());
        Assert.assertEquals(userId, projectRepository.findAll().get(0).getUserId());
        Assert.assertEquals(userId, projectRepository.findAll().get(1).getUserId());
    }

    @Test
    public void testFindAll() {
        projectRepository.create(userId, "name1");
        projectRepository.create(userId, "name2");
        projectRepository.create(userId2, "name3");
        Assert.assertEquals(2, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
        Assert.assertEquals(3, projectRepository.findAll().size());
    }

    @Test
    public void testFindAllSorted() {
        projectRepository.create(userId, "name8");
        projectRepository.create(userId, "name6");
        projectRepository.create(userId2, "name3");
        projectRepository.create(userId2, "name1");
        Assert.assertEquals("name6", projectRepository.findAll(userId, (Comparator<Project>) Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", projectRepository.findAll(userId2, (Comparator<Project>) Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    public void testFindOneByIndex() {
        projectRepository.create(userId, "name11");
        projectRepository.create(userId, "name12");
        Assert.assertEquals("name12", projectRepository.findOneByIndex(1).getName());
        Assert.assertEquals("name11", projectRepository.findOneByIndex(0).getName());
    }

    @Test
    public void testFindOneById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        Assert.assertEquals("name1", projectRepository.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", projectRepository.findOneById(userId, "id2").getName());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testExistsById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.existsById(userId, "id1"));
        Assert.assertTrue(projectRepository.existsById(userId, "id2"));
    }

    @Test
    public void testRemove() {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.remove(userId, project));
        Assert.assertFalse(projectRepository.findAll(userId).contains(project));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.removeByIndex(userId, 0));
        Assert.assertFalse(projectRepository.findAll(userId).contains(project));
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectRepository.add(userId, project);
        Assert.assertTrue(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.removeById(userId, "id1"));
        Assert.assertFalse(projectRepository.findAll(userId).contains(project));
        Assert.assertEquals(project, projectRepository.removeById(userId, "id1"));
    }

    @Test
    public void testTotalCount() {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        Assert.assertEquals(2, projectRepository.totalCount());
    }

    @Test
    public void testClear() {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectRepository.add(userId, project);
        projectRepository.add(userId, project2);
        @NotNull final Project project3 = new Project();
        project.setId("id3");
        project.setName("name3");
        projectRepository.add(userId2, project3);
        Assert.assertEquals(2, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
        projectRepository.clear(userId);
        Assert.assertEquals(0, projectRepository.findAll(userId).size());
        Assert.assertEquals(1, projectRepository.findAll(userId2).size());
    }

    @Test
    public void testRemoveAll() {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectRepository.add(projectList);
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.removeAll(projectList);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

}
