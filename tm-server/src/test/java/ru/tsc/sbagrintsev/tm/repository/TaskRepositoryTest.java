package ru.tsc.sbagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepositoryTest {

    @NotNull
    private final String userId = "testUserId1";
    
    @NotNull
    private final String userId2 = "testUserId2";
    
    @NotNull
    private TaskRepository taskRepository;

    @Before
    public void setUp() {
        taskRepository = new TaskRepository();
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final Task task1 = new Task();
        task1.setProjectId("project1");
        @NotNull final Task task2 = new Task();
        task2.setProjectId("project2");
        taskRepository.add(userId, task1);
        taskRepository.add(userId, task2);
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(userId, "project1").size());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testSetProjectId() throws AbstractException {
        @NotNull final Task task1 = new Task();
        task1.setId("task1");
        taskRepository.add(userId, task1);
        Assert.assertNull(taskRepository.findAll(userId).get(0).getProjectId());
        taskRepository.setProjectId(userId, "task1", "project1");
        Assert.assertEquals("project1", taskRepository.findAll(userId).get(0).getProjectId());
        taskRepository.setProjectId(userId, "task2", "project1");
    }

    @Test
    public void testAdd() {
        @NotNull final Task Task = new Task();
        taskRepository.add(Task);
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(Task, taskRepository.findAll().get(0));
    }

    @Test
    public void testAddCollection() {
        @NotNull final List<Task> TaskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        TaskList.add(task1);
        TaskList.add(task2);
        taskRepository.add(TaskList);
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals(2, taskRepository.totalCount());
        Assert.assertEquals(task2, taskRepository.findOneByIndex(1));
    }

    @Test
    public void testCreate() {
        taskRepository.create("id1", "name1");
        taskRepository.create("id2", "name2", "description2");
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertEquals("name1", taskRepository.findAll().get(0).getName());
        Assert.assertEquals("description2", taskRepository.findAll().get(1).getDescription());
    }

    @Test
    public void testFindAll() {
        taskRepository.create(userId, "name1");
        taskRepository.create(userId, "name2");
        taskRepository.create(userId2, "name3");
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
        Assert.assertEquals(3, taskRepository.findAll().size());
    }

    @Test
    public void testFindAllSorted() {
        taskRepository.create(userId, "name8");
        taskRepository.create(userId, "name6");
        taskRepository.create(userId2, "name3");
        taskRepository.create(userId2, "name1");
        Assert.assertEquals("name6", taskRepository.findAll(userId, (Comparator<Task>) Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", taskRepository.findAll(userId2, (Comparator<Task>) Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    public void testFindOneByIndex() {
        taskRepository.create(userId, "name11");
        taskRepository.create(userId, "name12");
        Assert.assertEquals("name12", taskRepository.findOneByIndex(1).getName());
        Assert.assertEquals("name11", taskRepository.findOneByIndex(0).getName());
    }

    @Test
    public void testFindOneById() throws AbstractException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        Assert.assertEquals("name1", taskRepository.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", taskRepository.findOneById(userId, "id2").getName());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testExistsById() throws AbstractException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.existsById(userId, "id1"));
        Assert.assertTrue(taskRepository.existsById(userId, "id2"));
    }

    @Test
    public void testRemove() {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.remove(userId, Task));
        Assert.assertFalse(taskRepository.findAll(userId).contains(Task));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.removeByIndex(userId, 0));
        Assert.assertFalse(taskRepository.findAll(userId).contains(Task));
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        taskRepository.add(userId, Task);
        Assert.assertTrue(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.removeById(userId, "id1"));
        Assert.assertFalse(taskRepository.findAll(userId).contains(Task));
        Assert.assertEquals(Task, taskRepository.removeById(userId, "id1"));
    }

    @Test
    public void testTotalCount() {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        Assert.assertEquals(2, taskRepository.totalCount());
    }

    @Test
    public void testClear() {
        @NotNull final Task Task = new Task();
        Task.setId("id1");
        Task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskRepository.add(userId, Task);
        taskRepository.add(userId, task2);
        @NotNull final Task Task3 = new Task();
        Task.setId("id3");
        Task.setName("name3");
        taskRepository.add(userId2, Task3);
        Assert.assertEquals(2, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
        taskRepository.clear(userId);
        Assert.assertEquals(0, taskRepository.findAll(userId).size());
        Assert.assertEquals(1, taskRepository.findAll(userId2).size());
    }

    @Test
    public void testRemoveAll() {
        @NotNull final List<Task> TaskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        TaskList.add(task1);
        TaskList.add(task2);
        taskRepository.add(TaskList);
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.removeAll(TaskList);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

}
