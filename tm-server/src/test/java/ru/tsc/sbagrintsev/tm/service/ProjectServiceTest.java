package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.bagrintsev.tm.enumerated.Status.IN_PROGRESS;
import static ru.tsc.bagrintsev.tm.enumerated.Status.NOT_STARTED;

public final class ProjectServiceTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private ProjectService projectService;

    @Before
    public void setUp() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Project project = new Project();
        projectService.add(userId, project);
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals(project, projectService.findAll().get(0));
        Assert.assertEquals(userId, projectService.findAll().get(0).getUserId());
    }

    @Test
    public void testAddCollection() throws IncorrectIndexException {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectService.add(projectList);
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertEquals(project2, projectService.findOneByIndex(1));
    }

    @Test
    public void testCreate() throws AbstractException {
        projectService.create(userId, "name1");
        projectService.create(userId, "name2", "description2");
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals("name1", projectService.findAll().get(0).getName());
        Assert.assertEquals("description2", projectService.findAll().get(1).getDescription());
        Assert.assertEquals(userId, projectService.findAll().get(0).getUserId());
        Assert.assertEquals(userId, projectService.findAll().get(1).getUserId());
    }

    @Test
    public void testFindAll() throws AbstractException {
        projectService.create(userId, "name1");
        projectService.create(userId, "name2");
        projectService.create(userId2, "name3");
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        Assert.assertEquals(3, projectService.findAll().size());
    }

    @Test
    public void testFindAllSorted() throws AbstractException {
        projectService.create(userId, "name8");
        projectService.create(userId, "name6");
        projectService.create(userId2, "name3");
        projectService.create(userId2, "name1");
        Assert.assertEquals("name6", projectService.findAll(userId, Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("name6", projectService.findAll(userId, (Comparator<Project>) Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", projectService.findAll(userId2, Sort.BY_CREATED).get(0).getName());
        Assert.assertEquals("name3", projectService.findAll(userId2, (Comparator<Project>) Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    public void testFindOneByIndex() throws AbstractException {
        projectService.create(userId, "name11");
        projectService.create(userId, "name12");
        Assert.assertEquals("name12", projectService.findOneByIndex(userId, 1).getName());
        Assert.assertEquals("name11", projectService.findOneByIndex(userId, 0).getName());
    }

    @Test
    public void testFindOneById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        Assert.assertEquals("name1", projectService.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", projectService.findOneById(userId, "id2").getName());
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        projectService.create(userId, "name12");
        Assert.assertEquals("name12", projectService.findOneByIndex(userId, 0).getName());
        Assert.assertNotNull(projectService.updateByIndex(userId, 0, "name13", "testDescription"));
        Assert.assertEquals("name13", projectService.findOneByIndex(userId, 0).getName());
        Assert.assertEquals("testDescription", projectService.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void testUpdateById() throws AbstractException {
        projectService.create(userId, "name12");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals("name12", projectService.findOneByIndex(userId, 0).getName());
        Assert.assertNotNull(projectService.updateById(userId, id, "name13", "testDescription"));
        Assert.assertEquals("name13", projectService.findOneByIndex(userId, 0).getName());
        Assert.assertEquals("testDescription", projectService.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void testChangeProjectStatusByIndex() throws AbstractException {
        projectService.create(userId, "name12");
        Assert.assertEquals(NOT_STARTED, projectService.findOneByIndex(userId, 0).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusByIndex(userId, 0, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, projectService.findOneByIndex(userId, 0).getStatus());
    }

    @Test
    public void testChangeProjectStatusById() throws AbstractException {
        projectService.create(userId, "name12");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals(NOT_STARTED, projectService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusById(userId, id, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, projectService.findOneById(userId, id).getStatus());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testExistsById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertTrue(projectService.existsById(userId, "id1"));
        Assert.assertTrue(projectService.existsById(userId, "id2"));
    }

    @Test
    public void testRemove() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertTrue(projectService.findAll(userId).contains(project));
        Assert.assertEquals(project, projectService.remove(userId, project));
        Assert.assertFalse(projectService.findAll(userId).contains(project));
    }

    @Test
    public void testRemoveByIndex() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertTrue(projectService.findAll(userId).contains(project));
        Assert.assertEquals(project, projectService.removeByIndex(userId, 0));
        Assert.assertFalse(projectService.findAll(userId).contains(project));
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        projectService.add(userId, project);
        Assert.assertTrue(projectService.findAll(userId).contains(project));
        Assert.assertEquals(project, projectService.removeById(userId, "id1"));
        Assert.assertFalse(projectService.findAll(userId).contains(project));
        Assert.assertEquals(project, projectService.removeById(userId, "id1"));
    }

    @Test
    public void testTotalCount() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        Assert.assertEquals(2, projectService.totalCount());
    }

    @Test
    public void testClear() throws AbstractException {
        @NotNull final Project project = new Project();
        project.setId("id1");
        project.setName("name1");
        @NotNull final Project project2 = new Project();
        project2.setId("id2");
        project2.setName("name2");
        projectService.add(userId, project);
        projectService.add(userId, project2);
        @NotNull final Project project3 = new Project();
        project.setId("id3");
        project.setName("name3");
        projectService.add(userId2, project3);
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        projectService.clear(userId);
        Assert.assertEquals(0, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
    }

    @Test
    public void testRemoveAll() {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        projectList.add(project1);
        projectList.add(project2);
        projectService.add(projectList);
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeAll(projectList);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

}
