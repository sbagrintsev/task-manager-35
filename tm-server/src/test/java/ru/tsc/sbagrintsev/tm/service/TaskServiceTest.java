package ru.tsc.sbagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.Task;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.service.TaskService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static ru.tsc.bagrintsev.tm.enumerated.Status.IN_PROGRESS;
import static ru.tsc.bagrintsev.tm.enumerated.Status.NOT_STARTED;

public final class TaskServiceTest {

    @NotNull
    private final String userId = "testUserId1";

    @NotNull
    private final String userId2 = "testUserId2";

    @NotNull
    private TaskService taskService;

    @Before
    public void setUp() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
    }

    @Test
    public void testAdd() throws AbstractException {
        @NotNull final Task task = new Task();
        taskService.add(userId, task);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(task, taskService.findAll().get(0));
        Assert.assertEquals(userId, taskService.findAll().get(0).getUserId());
    }

    @Test
    public void testAddCollection() throws IncorrectIndexException {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        taskList.add(task1);
        taskList.add(task2);
        taskService.add(taskList);
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertEquals(task2, taskService.findOneByIndex(1));
    }

    @Test
    public void testCreate() throws AbstractException {
        taskService.create(userId, "name1");
        taskService.create(userId, "name2", "description2");
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals("name1", taskService.findAll().get(0).getName());
        Assert.assertEquals("description2", taskService.findAll().get(1).getDescription());
        Assert.assertEquals(userId, taskService.findAll().get(0).getUserId());
        Assert.assertEquals(userId, taskService.findAll().get(1).getUserId());
    }

    @Test
    public void testFindAll() throws AbstractException {
        taskService.create(userId, "name1");
        taskService.create(userId, "name2");
        taskService.create(userId2, "name3");
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        Assert.assertEquals(3, taskService.findAll().size());
    }

    @Test
    public void testFindAllSorted() throws AbstractException {
        taskService.create(userId, "name8");
        taskService.create(userId, "name6");
        taskService.create(userId2, "name3");
        taskService.create(userId2, "name1");
        Assert.assertEquals("name6", taskService.findAll(userId, Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("name6", taskService.findAll(userId, (Comparator<Task>) Sort.BY_NAME.getComparator()).get(0).getName());
        Assert.assertEquals("name3", taskService.findAll(userId2, Sort.BY_CREATED).get(0).getName());
        Assert.assertEquals("name3", taskService.findAll(userId2, (Comparator<Task>) Sort.BY_CREATED.getComparator()).get(0).getName());
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException {
        @NotNull final Task task1 = new Task();
        task1.setProjectId("project1");
        @NotNull final Task task2 = new Task();
        task2.setProjectId("project2");
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAllByProjectId(userId, "project1").size());
    }

    @Test
    public void testFindOneByIndex() throws AbstractException {
        taskService.create(userId, "name11");
        taskService.create(userId, "name12");
        Assert.assertEquals("name12", taskService.findOneByIndex(userId, 1).getName());
        Assert.assertEquals("name11", taskService.findOneByIndex(userId, 0).getName());
    }

    @Test
    public void testFindOneById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals("name1", taskService.findOneById(userId, "id1").getName());
        Assert.assertEquals("name2", taskService.findOneById(userId, "id2").getName());
    }

    @Test
    public void testUpdateByIndex() throws AbstractException {
        taskService.create(userId, "name12");
        Assert.assertEquals("name12", taskService.findOneByIndex(userId, 0).getName());
        Assert.assertNotNull(taskService.updateByIndex(userId, 0, "name13", "testDescription"));
        Assert.assertEquals("name13", taskService.findOneByIndex(userId, 0).getName());
        Assert.assertEquals("testDescription", taskService.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void testUpdateById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals("name12", taskService.findOneByIndex(userId, 0).getName());
        Assert.assertNotNull(taskService.updateById(userId, id, "name13", "testDescription"));
        Assert.assertEquals("name13", taskService.findOneByIndex(userId, 0).getName());
        Assert.assertEquals("testDescription", taskService.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void testChangeTaskStatusByIndex() throws AbstractException {
        taskService.create(userId, "name12");
        Assert.assertEquals(NOT_STARTED, taskService.findOneByIndex(userId, 0).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusByIndex(userId, 0, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, taskService.findOneByIndex(userId, 0).getStatus());
    }

    @Test
    public void testChangeTaskStatusById() throws AbstractException {
        taskService.create(userId, "name12");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals(NOT_STARTED, taskService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusById(userId, id, IN_PROGRESS));
        Assert.assertEquals(IN_PROGRESS, taskService.findOneById(userId, id).getStatus());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testExistsById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.existsById(userId, "id1"));
        Assert.assertTrue(taskService.existsById(userId, "id2"));
    }

    @Test
    public void testRemove() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.findAll(userId).contains(task));
        Assert.assertEquals(task, taskService.remove(userId, task));
        Assert.assertFalse(taskService.findAll(userId).contains(task));
    }

    @Test
    public void testRemoveByIndex() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.findAll(userId).contains(task));
        Assert.assertEquals(task, taskService.removeByIndex(userId, 0));
        Assert.assertFalse(taskService.findAll(userId).contains(task));
    }

    @Test(expected = ModelNotFoundException.class)
    public void testRemoveById() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        taskService.add(userId, task);
        Assert.assertTrue(taskService.findAll(userId).contains(task));
        Assert.assertEquals(task, taskService.removeById(userId, "id1"));
        Assert.assertFalse(taskService.findAll(userId).contains(task));
        Assert.assertEquals(task, taskService.removeById(userId, "id1"));
    }

    @Test
    public void testTotalCount() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        Assert.assertEquals(2, taskService.totalCount());
    }

    @Test
    public void testClear() throws AbstractException {
        @NotNull final Task task = new Task();
        task.setId("id1");
        task.setName("name1");
        @NotNull final Task task2 = new Task();
        task2.setId("id2");
        task2.setName("name2");
        taskService.add(userId, task);
        taskService.add(userId, task2);
        @NotNull final Task task3 = new Task();
        task.setId("id3");
        task.setName("name3");
        taskService.add(userId2, task3);
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
    }

    @Test
    public void testRemoveAll() {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task task1 = new Task();
        @NotNull final Task task2 = new Task();
        taskList.add(task1);
        taskList.add(task2);
        taskService.add(taskList);
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAll(taskList);
        Assert.assertTrue(taskService.findAll().isEmpty());
    }
    
}
